#define _POSIX_C_SOURCE 200809L
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#define MIBI (1<<20)
#define GIBI (1<<30)

#define NTESTS 6

void analysis(struct timespec pre, struct timespec post, long bytes) {
        int sec,nsec;
        float s;

        sec = post.tv_sec - pre.tv_sec;
        nsec = post.tv_nsec - pre.tv_nsec;

        if (nsec < 0) {
                sec--;
                nsec += 1e9;
        }

        s = sec + nsec/1.0e9;

        if ( s > 0 ) {
                printf("%06.3f GB/s\n", bytes/(float)GIBI/s);
        }

        //printf("%d.%d s\n",sec, nsec);
}

int main() {
        int *p, *q;

        struct timespec pre, post;

        int tests[NTESTS] = {
                MIBI,
                MIBI*2,
                3e4,
                3e5,
                3e6,
                3e7
        };

        for(int i=0; i<NTESTS; ++i) {
                int size = tests[i]*sizeof(int);
                //printf("size: %d\n", size);
                //printf("mibi: %d\n", MIBI);
                p = (int *)malloc(size);
                q = (int *)malloc(size);

                printf("Allocating %.1f MB\n", (float)size/MIBI);

                printf("MEMSET\n");
                for(int j=0;j<4;++j) {
                        clock_gettime(CLOCK_REALTIME, &pre);
                        memset(p,0xAA, size);
                        clock_gettime(CLOCK_REALTIME, &post);
                        analysis(pre, post, size);
                }

                printf("MEMCPY\n");
                for(int j=0;j<4;++j) {
                        clock_gettime(CLOCK_REALTIME, &pre);
                        memcpy(p,q, size);
                        clock_gettime(CLOCK_REALTIME, &post);
                        analysis(pre, post, size);
                }
                free(q);
                free(p);
        }
}
