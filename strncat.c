#include <string.h>
#include <stdio.h>

#define BUF_SIZE 3



int main(void) {
        printf("BUF_SIZE is %d\n", BUF_SIZE);

        char before[BUF_SIZE] = "aaa";
        char buf[BUF_SIZE];
        char after[BUF_SIZE] = "bbb";
        char src[] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

        /* TEST */
        buf[0] = '\0';
        strncat(buf, src, sizeof(buf)-1);
        printf("strncat buf : %s, %s\n", buf, src);

        /* TEST */
        buf[0] = '\0';
        strncat(buf, src, sizeof(buf));
        printf("strncat buf : %s, after : %s, before : %s\n", buf, after, before);

        /* TEST */
        strncpy(buf, src, sizeof(buf));
        printf("strncpy buf : %s, after : %s, before : %s\n", buf, after, before);

        /* TEST */
        buf[0] = '\0';
        strncat(buf, src, sizeof(buf)+20);
        printf("strncat buf : %s, after : %s, before : %s\n", buf, after, before);
}
