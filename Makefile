all: memtest cmptest

CFLAGS = --std=c99 -Wall -pedantic -g -pg -O3 -fno-inline -march=core-avx2

memtest: memtest.c
	gcc -o $@ ${CFLAGS} $^ -lrt

cmptest: cmptest.c
	gcc -o $@ ${CFLAGS} -Wl,-rpath=/usr/local/epics/base/lib/linux-x86_64 -I/usr/local/epics/base/include -L/usr/local/epics/base/lib/linux-x86_64 $^ -lrt -lCom

run: memtest
	./memtest

clean:
	rm -f memtest cmptest
