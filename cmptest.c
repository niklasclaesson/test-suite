#define _POSIX_C_SOURCE 200809L
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include <epicsString.h>

#define MIBI (1<<20)
#define GIBI (1<<30)

#define NTESTS 6

void analysis(struct timespec pre, struct timespec post, long bytes) {
        int sec,nsec;
        float s;

        sec = post.tv_sec - pre.tv_sec;
        nsec = post.tv_nsec - pre.tv_nsec;

        if (nsec < 0) {
                sec--;
                nsec += 1e9;
        }

        s = sec + nsec/1.0e9;

        if ( s > 0 ) {
                printf("%06.3f GB/s\n", bytes/(float)GIBI/s);
        }

        //printf("%d.%d s\n",sec, nsec);
}

int main() {
        int *p, *q;

        unsigned int hash = 0, new_hash = 0;

        struct timespec pre, post;

        int tests[NTESTS] = {
                MIBI,
                MIBI*2,
                3e4,
                3e5,
                3e6,
                3e7
        };

        for(int i=0; i<NTESTS; ++i) {
                int size = tests[i]*sizeof(int);
                int j = 0, status;
                //printf("size: %d\n", size);
                //printf("mibi: %d\n", MIBI);
                p = (int *)malloc(size);
                q = (int *)malloc(size);

#if 0
                for(j = 0; j<size/4; ++j) {
                        p[j] = rand();
                        q[j] = rand();
                }
#endif

                printf("Allocating %.1f MB\n", (float)size/MIBI);

                printf("MEMCMP\n");
                for(j=0;j<4;++j) {
                        clock_gettime(CLOCK_REALTIME, &pre);
                        status = memcmp((char *)p, (char *)q, size);
                        clock_gettime(CLOCK_REALTIME, &post);
                        printf("%d ", status);
                        analysis(pre, post, size);
                }
#if 1
                printf("Hash\n");
                for(j=0;j<4;++j) {
                        clock_gettime(CLOCK_REALTIME, &pre);
                        new_hash = epicsMemHash((char *)p, size, 0);
                        clock_gettime(CLOCK_REALTIME, &post);
                        printf("%d ", hash != new_hash);
                        hash = new_hash;
                        analysis(pre, post, size);
                }
#endif
                free(q);
                free(p);
        }
}
